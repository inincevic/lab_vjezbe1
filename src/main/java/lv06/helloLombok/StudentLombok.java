package lv06.helloLombok;

import lombok.*;

import java.math.BigDecimal;

@Builder
@ToString
@Getter
@NoArgsConstructor // added for mapper.readValue in lv06/HelloSerialization
@AllArgsConstructor // added for mapper.readValue in lv06/HelloSerialization
public class StudentLombok {
    String name;
    Integer yearOfStudy;
    BigDecimal averageGrade;
    Integer birthYear;
    String phoneNumber;

    public static void main(String[] args) {
        StudentLombok student = StudentLombok.builder()
                .name("Pero")
                .averageGrade(new BigDecimal(5.0))
                .yearOfStudy(5)
                .build();

        System.out.println(student);

        System.out.println("Ime studenta: " + student.getName());
    }
}
