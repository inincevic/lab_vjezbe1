package lv02.z02;

import javax.swing.*;
import java.awt.*;

public class ButtonClick {
    private JFrame frame;
    private JButton button;
    private JLabel label;

    private static int clickCounter = 0;

    ButtonClick() {
        frame = new JFrame("LV02-Z01");
        frame.setSize(700, 400);

        button = new JButton();
        button.setText("Click Me");

        Rectangle buttonRectangle = new Rectangle(50, 50, 100, 100);
        button.setBounds(buttonRectangle); // another way to set bounds

        label = new JLabel(String.valueOf(clickCounter));
        label.setBounds(200, 50, 100, 100);
        label.setBackground(Color.CYAN);
        label.setOpaque(true);
        label.setFont(new Font("Serif", Font.PLAIN, 20));
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setVerticalAlignment(SwingConstants.CENTER);

        // equals to
        // button.addActionListener(new ActionListener() {new ActionListener() {
        //        @Override
        //        public void actionPerformed(ActionEvent e) {
        //        }
        //});
        button.addActionListener(e -> {
            clickCounter++;
            label.setText(clickCounter + "");
        });

        frame.add(button);
        frame.add(label);
        frame.setLayout(null); // null layout means absolute positioning. Not using layout manager
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void show() {
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new ButtonClick().show());
    }


}
