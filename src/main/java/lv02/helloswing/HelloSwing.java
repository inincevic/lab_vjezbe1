package lv02.helloswing;

import javax.swing.*;

public class HelloSwing {
    private JFrame frame;
    private JLabel label;

    HelloSwing(String[] args) {
        // Do any initialization
        frame = new JFrame();
        frame.setTitle("Hello Swing");

        label = new JLabel();
        label.setBounds(100, 200, 200, 30);
        label.setText("First label using Swing");

        frame.add(label);
        frame.setSize(700, 500);
        frame.setLayout(null); // null layout means absolute positioning. Not using layout manager
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void show() {
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // spring is not thread safe
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new HelloSwing(args).show();
            }
        });
    }
}
