package lv02.helloswing;

import javax.swing.*;
import java.awt.*;

public class HelloTextField {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            JFrame frame = new JFrame("TextField example");
            frame.setSize(400, 400);

            TextField textField = new TextField();
            textField.setBounds(50, 100, 250, 30);
            textField.setText("Hello textField");

            frame.add(textField);
            frame.setLayout(null);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}
