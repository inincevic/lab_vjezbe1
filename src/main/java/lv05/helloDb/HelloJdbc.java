package lv05.helloDb;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

public class HelloJdbc {
    public static void main(String[] args) {
        // jdbc:sqlserver://[serverName[\instanceName][:portNumber]][;property=value[;property=value]]
        String databaseUrl = "jdbc:sqlserver://localhost:1433;user=rpuser;password=1234";

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(databaseUrl); // TODO: prikazati na koju bazu smo se upravo spojili
            System.out.println("Connected");

            DatabaseMetaData meta = conn.getMetaData();
            System.out.println("Driver: " + meta.getDriverName() + " " + meta.getDriverVersion());
            System.out.println("Connection: " + meta.getConnection());
            System.out.println("Product version: " + meta.getDatabaseProductVersion());

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            try {
                if(conn != null && conn.isClosed())
                    conn.close();
            } catch (SQLException e) {
                System.out.println("Error closing connection: " + e.getMessage());
            }
        }
    }
}
