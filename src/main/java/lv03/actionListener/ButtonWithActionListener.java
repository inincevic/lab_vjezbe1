package lv03.actionListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonWithActionListener extends JPanel implements ActionListener {
    JButton button;

    public ButtonWithActionListener() {
        super(new BorderLayout());
        button = new JButton("Click Me");
        add(button, BorderLayout.CENTER);
        button.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Button Clicked");
    }
}
