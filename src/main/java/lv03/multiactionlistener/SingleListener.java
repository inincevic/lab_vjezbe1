package lv03.multiactionlistener;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SingleListener implements ActionListener {
    JTextArea textArea;

    public SingleListener(JTextArea textArea) {
        this.textArea = textArea;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        textArea.append(e.getActionCommand() + "\n");
        textArea.setCaretPosition(textArea.getDocument().getLength());
    }
}