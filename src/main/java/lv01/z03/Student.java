package lv01.z03;

import java.math.BigDecimal;

public class Student {
    private String name;
    private int yearOfStudy;
    private BigDecimal averageGrade;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public BigDecimal getAverageGrade() {
        return averageGrade;
    }

    public void setAverageGrade(BigDecimal averageGrade) {
        this.averageGrade = averageGrade;
    }

    // enkapsulacija omogucuje zastitu atributa klase. U ovom primjeru imamo private atribute te za dohvat koristimo gettere, a za postavljanje settere
    public static void main(String[] args) {
        Student student = new Student();
        student.setName("Pero Peric");
        student.setYearOfStudy(3);
        student.setAverageGrade(BigDecimal.valueOf(5.0));

        StringBuilder sb = new StringBuilder();
        sb.append("Ime: ");
        sb.append(student.getName());
        sb.append("\n");
        sb.append("Godina studija: ");
        sb.append(student.getYearOfStudy());
        sb.append("\n");
        sb.append("Prosjecna ocijena: ");
        sb.append(student.getAverageGrade().toPlainString());

        System.out.println(sb);

        // or
        System.out.println("Ime:" + student.getName());
        System.out.println("Godina studija::" + student.getYearOfStudy());
        System.out.println("Prosjecna ocijena:" + student.getAverageGrade());
    }
}
